﻿using System;
using PK.Container;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            var container =new Container();
            container.Register(new Lab5.DisplayForm.DisplayViewModel());
            container.Register(LabDescriptor.DisplayComponentImpl);
            container.Register(LabDescriptor.MainComponentImpl);
            return container;
        }
    }
}

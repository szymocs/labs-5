﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab5.Główny.Kontrakt;
using Lab5.Wyświetlacz.Kontrakt;

namespace Lab5.Główny.Implemetacja
{
  public  class Glowny:IGlowny
    {
      IWyswietlacz display;
      public Glowny(IWyswietlacz displayint)
      {
          this.display = displayint;
      }


      void IGlowny.Dzialanie()
      {
          display.Myjnia();
          display.Myje();
          display.Czysci();
          display.Suszy();
          display.Koniec();
      }
    }
}

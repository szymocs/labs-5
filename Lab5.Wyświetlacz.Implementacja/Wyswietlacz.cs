﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Lab5.Wyświetlacz.Kontrakt;
using Lab5.DisplayForm;
using System.Threading;

namespace Lab5.Wyświetlacz.Implementacja
{
   public class Wyswietlacz:IWyswietlacz
    {
       DisplayViewModel model;
       Form form;
       DisplayViewModel viewModel;
       public Wyswietlacz()
       {
            model = new DisplayViewModel();
            form = new Form();
            viewModel = new DisplayViewModel();
       }
       
        void IWyswietlacz.Myjnia()
        {
            /* Uruchomienie kodu w wątku aplikacji okienkowej */
            model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                // utworzenie nowej formatki graficznej stanowiącej widok
                form = new Form();
                // utworzenie modelu widoku (wzorzec MVVM)
                 viewModel = new DisplayViewModel();
                // przypisanie modelu do widoku
                form.DataContext = viewModel;
                // wyświetlenie widoku
                form.Show();
                // zwrócenie modelu widoku do dalszych manipulacji
                return viewModel;
            }), null);

            /* Modyfikacja właściwości modelu - zmiana napisu na wyświetlaczu */
            model.Text = "Myjnia";
            Thread.Sleep(1000);
            model.Text = "Myjnia2";
        }

        void IWyswietlacz.Myje()
        {
           

            /* Modyfikacja właściwości modelu - zmiana napisu na wyświetlaczu */
            model.Text = "Myje";
            Thread.Sleep(1000);
            
        }

        void IWyswietlacz.Czysci()
        {
           

            /* Modyfikacja właściwości modelu - zmiana napisu na wyświetlaczu */
            model.Text = "Czyści";
            Thread.Sleep(1000);
            
        }

        void IWyswietlacz.Suszy()
        {
            

            /* Modyfikacja właściwości modelu - zmiana napisu na wyświetlaczu */
            model.Text = "Suszy";
            Thread.Sleep(1000);
           
        }





        void IWyswietlacz.Koniec()
        {
            model.Text = "Koniec";
        }
    }
}
